# -*- coding: utf-8 -*-
'''
© 2012-2013 eBay Software Foundation
Authored by: Tim Keefer
Licensed under CDDL 1.0
'''

import os
import sys
import csv
import datetime
from optparse import OptionParser

sys.path.insert(0, '%s/../' % os.path.dirname(__file__))

from common import dump

import ebaysdk
from ebaysdk.utils import getNodeText
from ebaysdk.exception import ConnectionError
from ebaysdk.trading import Connection as Trading
from ebaysdk.soa.finditem import Connection as FindItem
from ebaysdk.shopping import Connection as Shopping


def init_options():
    usage = "usage: %prog [options]"
    parser = OptionParser(usage=usage)

    parser.add_option("-d", "--debug",
                      action="store_true", dest="debug", default=False,
                      help="Enabled debugging [default: %default]")
    parser.add_option("-y", "--yaml",
                      dest="yaml", default='ebay.yaml',
                      help="Specifies the name of the YAML defaults file. [default: %default]")
    parser.add_option("-a", "--appid",
                      dest="appid", default=None,
                      help="Specifies the eBay application id to use.")
    parser.add_option("-p", "--devid",
                      dest="devid", default=None,
                      help="Specifies the eBay developer id to use.")
    parser.add_option("-c", "--certid",
                      dest="certid", default=None,
                      help="Specifies the eBay cert id to use.")

    (opts, args) = parser.parse_args()
    return opts, args

def GetItemTransaction(opts, num):

    api = Trading(debug=opts.debug, config_file=opts.yaml, appid=opts.appid,
                  certid=opts.certid, devid=opts.devid, warnings=True, timeout=20)

    api.execute('GetItemTransactions', {'ItemID': num})
    dictstr = api.response.dict()
    return dictstr

def GetSellerList(opts):

    api = Trading(debug=opts.debug, config_file=opts.yaml, appid=opts.appid,
                  certid=opts.certid, devid=opts.devid, warnings=True, timeout=20)

    api.execute('GetSellerList', {'UserID': 'hubcaps_plus', 'StartTimeFrom': '2017-01-01T21:59:59.005Z', 'StartTimeTo': '2017-03-07T21:59:59.005Z'})
    dictstr = api.response.dict()
    return dictstr

def getRows(str):
    return len(str) - len(str.replace('\n', ''))



    
(opts, args) = init_options()
Info = GetSellerList(opts)


with open('newtest.csv', 'wb') as csvFile:
    writer = csv.writer(csvFile, delimiter=',')
    writer.writerow(['ItemID'])
    for value in Info['ItemArray'].values():
        items = []
        items.append(value)
        for item in items:
            for thing in item:
                identifier = thing
                try:
                    ItemInfo = GetItemTransaction(opts, num)
                    amountsold = ItemInfo['Item']['SellingStatus']['QuantitySold']
                    SKU = ItemInfo['Item']['SKU']
                    TITLE = ItemInfo['Item']['Title']
                except:
                    pass
                product = []
                product.append(identifier.values()[0], amountsold, SKU, TITLE)
                print product




























